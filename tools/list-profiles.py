#!/usr/bin/env python3
import argparse, json
from common import FirefoxProfiles

# Parse our command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument('--default', action='store_true', help='Only print the details of the default profile, rather than listing all profiles')
parser.add_argument('--paths-only', action='store_true', help='Only print the filesystem path for each profile, rather than the full details')
args = parser.parse_args()

# Retrieve the list of Firefox profiles for the current user
profiles = FirefoxProfiles.list_all()

# Skip other profiles if we've been instructed to just print the details of the default profile
if args.default == True:
	profiles = [p for p in profiles if p['default'] == True]

# Determine whether we are printing the full details of each profile or just the filesystem path
if args.paths_only == True:
	print('\n'.join([p['path'] for p in profiles]))
else:
	print(json.dumps(profiles, indent=4))
