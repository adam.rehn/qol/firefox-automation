from .utility import Utility

class FirefoxFunctions:
	"""
	Contains functions from the Firefox source code, ported from C++ to Python
	"""
	
	@staticmethod
	def RotateLeft5(value):
		"""
		Python implementation of `mozilla::detail::RotateLeft5()`
		
		Original C++ implementation here:
		https://github.com/mozilla/gecko-dev/blob/esr102/mfbt/HashFunctions.h#L104-L107
		"""
		return (value << 5) | (value >> 27)
	
	@staticmethod
	def WrappingMultiply(a, b):
		"""
		Python implementation of `mozilla::WrappingMultiply<uint32_t>()`
		
		Original C++ implementation here:
		<https://github.com/mozilla/gecko-dev/blob/esr102/mfbt/WrappingOperations.h#L204-L258>
		"""
		return Utility.to_u32(a * b)
	
	@staticmethod
	def AddU32ToHash(hash, value):
		"""
		Python implementation of `mozilla::detail::AddU32ToHash()`
		
		Original C++ implementation here:
		<https://github.com/mozilla/gecko-dev/blob/esr102/mfbt/HashFunctions.h#L109-L153>
		"""
		kGoldenRatioU32 = 0x9e3779b9
		return FirefoxFunctions.WrappingMultiply(kGoldenRatioU32, FirefoxFunctions.RotateLeft5(hash) ^ value)
	
	@staticmethod
	def HashString(str):
		"""
		Python implementation of `mozilla::HashStringKnownLength()`
		
		Original C++ implementation here:
		<https://github.com/mozilla/gecko-dev/blob/esr102/mfbt/HashFunctions.h#L257-L268>
		"""
		hash = 0
		for currentByte in str.encode('utf-8'):
			hash = FirefoxFunctions.AddU32ToHash(hash, currentByte)
		return hash
	
	@staticmethod
	def HashURL(url):
		"""
		Python implementation of `mozilla::places::HashURL()`
		
		Original C++ implementation here:
		<https://github.com/mozilla/gecko-dev/blob/esr102/toolkit/components/places/Helpers.cpp#L255-L277>
		"""
		if ':' in url:
			prefix = url[0 : url.index(':')]
			prefixHash = FirefoxFunctions.HashString(prefix) & 0x0000ffff
			return (prefixHash << 32) + FirefoxFunctions.HashString(url)
		else:
			return FirefoxFunctions.HashString(url)
	
	@staticmethod
	def FixupURL(url):
		"""
		Python implementation of `mozilla::places::FixupURLFunction::OnFunctionCall()`
		
		Original C++ implementation here:
		<https://github.com/mozilla/gecko-dev/blob/esr102/toolkit/components/places/SQLFunctions.cpp#L849-L875>
		"""
		for prefix in ['http://', 'https://', 'ftp://', 'www.']:
			if url.startswith(prefix):
				url = url[len(prefix):]
		return url
