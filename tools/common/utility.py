import json
from pathlib import Path

class Utility:
	"""
	Provides assorted utility functionality
	"""
	
	@staticmethod
	def parse_json(filename):
		"""
		Parses a JSON file, wrapping any errors with a human-friendly message prefix
		"""
		try:
			with open(filename, 'r') as f:
				return json.load(f)
		except RuntimeError as err:
			raise RuntimeError('Failed to parse JSON file "{}": {}'.format(filename, err))
	
	@staticmethod
	def read_file(filename):
		"""
		Reads the contents of a UTF-8 encoded text file
		"""
		return Path(filename).read_bytes().decode('utf-8')
	
	@staticmethod
	def to_u32(num):
		"""
		Casts a number to an unsigned 32-bit integer.
		Adapted from the constructor of the class in this example code: <https://stackoverflow.com/a/20430648>
		"""
		return num % 2**32
