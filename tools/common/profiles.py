import configparser
from pathlib import Path

class FirefoxProfiles:
	"""
	Provides functionality for querying the Firefox profiles for the current user
	"""
	
	@staticmethod
	def firefox_config_dir():
		"""
		Determines the location of the Firefox configuration data directory for the current user
		"""
		
		# TODO: implement this for platforms other than Linux
		return Path('~/.mozilla/firefox').expanduser()
	
	@staticmethod
	def get_default():
		"""
		Retrieves the details of the default Firefox profile for the current user
		"""
		profiles = FirefoxProfiles.list_all()
		default = [p for p in profiles if p['default'] == True]
		return default[0]
	
	@staticmethod
	def list_all():
		"""
		Lists the details of all available Firefox profiles for the current user
		"""
		
		# Retrieve the path to the Firefox configuration data directory
		configDir = FirefoxProfiles.firefox_config_dir()
		
		# Parse the profiles.ini file
		config = configparser.ConfigParser()
		config.read(configDir / 'profiles.ini')
		
		# Identify the sections that describe installations and profiles
		installSections = [s for s in config.sections() if s.startswith('Install')]
		profileSections = [s for s in config.sections() if s.startswith('Profile')]
		
		# Retrieve the name of the default profile for the first installation
		firstInstall = installSections[0]
		defaultProfile = config[firstInstall]['Default']
		
		# Process the list of profiles
		profiles = []
		for section in profileSections:
			profiles.append({
				'name': config[section]['Name'],
				'path': str(configDir / config[section]['Path']),
				'default': config[section]['Path'] == defaultProfile
			})
		
		return profiles
