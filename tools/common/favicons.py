import urllib.request, sqlite3
from .firefox_funcs import FirefoxFunctions

class FaviconUpdater:
	"""
	Provides functionality for updating the Firefox favicon cache
	"""
	
	def __init__(self, database_file):
		"""
		Attempts to open the specified SQLite database
		"""
		self._database_file = database_file
		self._connection = sqlite3.connect(self._database_file)
	
	def get_max_expiration(self):
		"""
		Returns the maximum value that an expiration timestamp can have,
		which is the maximum value of a signed 64-bit integer
		"""
		return 9223372036854775807
	
	def close(self):
		"""
		Closes the SQLite database connection
		"""
		self._connection.close()
	
	def commit(self):
		"""
		Commits the transaction for the SQLite database connection
		"""
		self._connection.commit()
	
	def rollback(self):
		"""
		Rolls back the transaction for the SQLite database connection
		"""
		self._connection.rollback()
	
	def get_icon_hash(self, icon):
		"""
		Computes the hash for the specified favicon URL
		"""
		return FirefoxFunctions.HashURL(FirefoxFunctions.FixupURL(icon))
	
	def get_icon_id(self, icon):
		"""
		Retrieves the ID for the favicon with the specified URL (if any)
		"""
		hash = self.get_icon_hash(icon)
		existing = self._connection.execute('SELECT id FROM moz_icons WHERE fixed_icon_url_hash = ?', [hash]).fetchone()
		return existing[0] if existing is not None else None
	
	def get_site_hash(self, site):
		"""
		Computes the hash for the specified website URL
		"""
		return FirefoxFunctions.HashURL(site)
	
	def get_site_id(self, site):
		"""
		Retrieves the ID for the website with the specified URL (if any)
		"""
		hash = self.get_site_hash(site)
		existing = self._connection.execute('SELECT id FROM moz_pages_w_icons WHERE page_url_hash = ?', [hash]).fetchone()
		return existing[0] if existing is not None else None
	
	def download_icon(self, url):
		"""
		Downloads the favicon from the specified URL, returning both the blob and the MIME type
		"""
		response = urllib.request.urlopen(url)
		return {
			'mime': response.getheader('Content-type'),
			'blob': response.read()
		}
	
	def set_icon_for_site(self, site, icon):
		"""
		Sets the favicon for a website
		"""
		
		# Attempt to download the favicon, and verify that it is in either PNG or SVG format
		iconDetails = self.download_icon(icon)
		if iconDetails['mime'] not in ['image/png', 'image/svg+xml']:
			raise RuntimeError('Only PNG and SVG icons are supported!')
		
		# Retrieve the ID for the favicon if we already have a row for it
		iconID = self.get_icon_id(icon)
		
		# If we don't have a row for the favicon then create one
		if iconID is None:
			self._connection.execute('INSERT INTO moz_icons (icon_url, fixed_icon_url_hash) VALUES (?, ?)', [icon, self.get_icon_hash(icon)])
			iconID = self.get_icon_id(icon)
		
		# Update the row for the favicon with its current details
		self._connection.execute('UPDATE moz_icons SET width = :width, root = 0, color = NULL, expire_ms = :expiration, data = :data WHERE id = :id', {
			'id':         iconID,
			'width':      65535,                   # NOTE: width = 65535 for SVG files, TODO: compute the actual width for PNG files
			'data':       iconDetails['blob'],
			'expiration': self.get_max_expiration()
		})
		
		# Retrieve the ID for the website URL if we already have a row for it
		siteID = self.get_site_id(site)
		
		# If we don't have a row for the website then create one
		if siteID is None:
			self._connection.execute('INSERT INTO moz_pages_w_icons (page_url, page_url_hash) VALUES (?, ?)', [site, self.get_site_hash(site)])
			siteID = self.get_site_id(site)
		
		# Remove any existing favicon mappings for the website
		self._connection.execute('DELETE FROM moz_icons_to_pages WHERE page_id = :siteID', [siteID])
		
		# Create a new favicon mapping for the website
		self._connection.execute('INSERT INTO moz_icons_to_pages (page_id, icon_id, expire_ms) VALUES (?, ?, ?)', [siteID, iconID, self.get_max_expiration()])
