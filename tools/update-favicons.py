#!/usr/bin/env python3
import argparse, sys
from common import FaviconUpdater, FirefoxProfiles, Utility
from pathlib import Path

# Parse our command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument('--url', default=None, help='The URL of the website for which the favicon data will be set')
parser.add_argument('--icon', default=None, help='The URL of the favicon that will be set for the website')
parser.add_argument('--file', default=None, help='Specifies a JSON file containing website and favicon URLs')
args = parser.parse_args()

try:
	
	# Ensure that either a URL+icon pair was supplied, or else a JSON file was specified
	if args.file is None and (args.url is None or args.icon is None):
		raise RuntimeError('You must either specify a JSON file with --file, or else specify both --url and --icon')
	
	# Ensure that a URL+icon pair was not supplied if a JSON file was specified
	if args.file is not None and args.url is not None and args.icon is not None:
		raise RuntimeError('You must not specify --url or --icon if you have specified a JSON file with --file')
	
	# Retrieve the website and icon details from the specified inputs
	iconPairs = []
	if args.file is not None:
		iconPairs = Utility.parse_json(args.file)
		if not isinstance(iconPairs, list) or len([p for p in iconPairs if 'url' in p and 'icon' in p]) == 0:
			raise RuntimeError('Input JSON file is malformed! It should contain an array of objects with the keys "url" and "icon"!')
	else:
		iconPairs.append({
			'url':  args.url,
			'icon': args.icon
		})
	
	# Locate the favicons SQLite database file for the default Firefox profile
	profile = Path(FirefoxProfiles.get_default()['path'])
	database = profile / 'favicons.sqlite'
	
	# Verify that Firefox is not currently running
	sentinel = profile / 'favicons.sqlite-wal'
	if sentinel.exists():
		raise RuntimeError('Firefox is currently running! Please ensure Firefox is closed before running this script.')
	
	# Attempt to apply our updates to the favicons database
	updater = FaviconUpdater(database)
	try:
		for site in iconPairs:
			print('Setting favicon for "{}" to "{}"...'.format(site['url'], site['icon']), flush=True)
			updater.set_icon_for_site(site['url'], site['icon'])
		updater.commit()
	except:
		updater.rollback()
		raise
	finally:
		updater.close()
	
except Exception as err:
	print('Error: {}'.format(err), file=sys.stderr, flush=True)
	sys.exit(1)
