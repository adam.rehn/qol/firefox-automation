# Configuration automation tools for Firefox

This repository contains assorted tools to automate the process of configuring and tweaking the [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/) web browser. The tools are all implemented as Python scripts, and require Python 3.7 or newer in order to run. Note that these tools are primarily tested under Linux, and may not function 100% correctly on other platforms.

The following tools are currently available:

- [**list-profiles.py**](./tools/list-profiles.py): reports information about the available Firefox profiles for the current user. This is primarily useful for determining the location of the default profile so its settings can then be configured programmatically.

- [**update-favicons.py**](./tools/update-favicons.py): overwrites entries in the Firefox favicon cache with icon data specified by the user. This is primarily useful for esoteric use cases such as assigning icons to websites that don't have a favicon, ensuring icons will be displayed for any bookmarks to those websites.


## Legal

Copyright &copy; 2022, Adam Rehn. Licensed under the Mozilla Public License (MPL) Version 2.0, see the file [LICENSE](./LICENSE) for details.

The file [tools/common/firefox_funcs.py](./tools/common/firefox_funcs.py) contains Python implementations of C++ functions from the Mozilla Firefox source code. Each function that has been ported includes a comment specifying both the name of the original C++ function and the link to the original implementation. The Firefox source code is Copyright &copy; the Mozilla Foundation and is licensed under the [Mozilla Public License Version 2.0](https://www.mozilla.org/en-US/MPL/2.0/). For more details, see the `about:license` page in Firefox, or the [license template file](https://github.com/mozilla/gecko-dev/blob/master/toolkit/content/license.html) used to generate that page.
